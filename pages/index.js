import Head from 'next/head'
import Banner from '../components/Banner'

export default function Home() {
  const data = {
    title: "Budget Expenses Tracker",
    content: "Guard your savings"
  }
  return (
    <>
      <Head>
        <title>Budget Expenses Tracker</title>
      </Head>
      <Banner data={data} />
    </>
  )
}